package com.myst3ry.mvpatternspractice.mvvm.view;

import com.myst3ry.mvpatternspractice.mvvm.databinding.ObservableDailyForecast;

import java.util.List;

public interface MainView {

    void setForecasts(final List<ObservableDailyForecast> forecasts);

    void showProgressBar();

    void hideProgressBar();

    void hideRefresher();
}

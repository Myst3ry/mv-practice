package com.myst3ry.mvpatternspractice.mvvm.view;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.myst3ry.mvpatternspractice.R;
import com.myst3ry.mvpatternspractice.mvp.view.DetailActivity;
import com.myst3ry.mvpatternspractice.mvvm.ForecastAdapter;
import com.myst3ry.mvpatternspractice.mvvm.MainPresenter;
import com.myst3ry.mvpatternspractice.mvvm.databinding.ObservableDailyForecast;
import com.myst3ry.mvpatternspractice.utils.LinearSpacingItemDecoration;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.weather_rec_view)
    RecyclerView mWeatherRecyclerView;
    @BindView(R.id.refresher)
    SwipeRefreshLayout mRefresher;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private ForecastAdapter mAdapter;
    private MainPresenter<MainActivity> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initPresenter();
        initAdapter();
        initRecyclerView();
        setRefreshListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getForecasts();
    }

    private void initPresenter() {
        mPresenter = new MainPresenter<>();
        mPresenter.attachView(this);
    }

    private void initAdapter() {
        mAdapter = new ForecastAdapter(date -> startActivity(DetailActivity.newExtraIntent(MainActivity.this, date)));
    }

    private void initRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mWeatherRecyclerView.setLayoutManager(layoutManager);
        mWeatherRecyclerView.addItemDecoration(LinearSpacingItemDecoration.newBuilder()
                .spacing(getResources().getDimensionPixelSize(R.dimen.margin_half))
                .orientation(LinearLayoutManager.VERTICAL)
                .includeEdge(true)
                .build());
        mWeatherRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void setForecasts(List<ObservableDailyForecast> forecasts) {
        mAdapter.setForecastList(forecasts);
    }

    @Override
    public void showProgressBar() {
        if (mProgressBar != null && mProgressBar.getVisibility() == View.GONE) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressBar() {
        if (mProgressBar != null && mProgressBar.getVisibility() == View.VISIBLE) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void setRefreshListener() {
        mRefresher.setOnRefreshListener(() -> mPresenter.getForecastsUpdates());
    }

    @Override
    public void hideRefresher() {
        if (mRefresher != null && mRefresher.isRefreshing()) {
            mRefresher.setRefreshing(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isDestroyed()) {
            mPresenter.detachView();
        }
    }
}

package com.myst3ry.mvpatternspractice.mvvm.databinding;

import com.myst3ry.mvpatternspractice.data.local.model.ForecastModel;

import java.util.ArrayList;
import java.util.List;

public final class ObservableMapper {

    public static ObservableDailyForecast transform(final ForecastModel model) {
        return new ObservableDailyForecast(
                model.getEpochDate(),
                model.getDate(),
                model.getTemperatureMax(),
                model.getTemperatureMin(),
                model.getUnit(),
                model.getDayPhrase(),
                model.getNightPhrase(),
                model.getCity()
        );
    }

    public static List<ObservableDailyForecast> transform(final List<ForecastModel> models) {
        final List<ObservableDailyForecast> observableForecasts = new ArrayList<>();
        for (final ForecastModel modelForecast : models) {
            observableForecasts.add(transform(modelForecast));
        }
        return observableForecasts;
    }

    private ObservableMapper() {
    }
}

package com.myst3ry.mvpatternspractice.mvvm.databinding;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;
import android.support.annotation.NonNull;

import com.myst3ry.mvpatternspractice.BR;

public final class ObservableDailyForecast implements Observable {

    private int mEpochDate;
    private String mDate;
    private int mTemperatureMax;
    private int mTemperatureMin;
    private String mUnit;
    private String mDayPhrase;
    private String mNightPhrase;
    private String mCity;

    private final PropertyChangeRegistry mRegistry = new PropertyChangeRegistry();

    public ObservableDailyForecast(int epochDate, String date, int temperatureMax, int temperatureMin,
                              String tempUnit, String dayPhrase, String nightPhrase, String city) {
        this.mEpochDate = epochDate;
        this.mDate = date;
        this.mTemperatureMax = temperatureMax;
        this.mTemperatureMin = temperatureMin;
        this.mUnit = tempUnit;
        this.mDayPhrase = dayPhrase;
        this.mNightPhrase = nightPhrase;
        this.mCity = city;
    }

    @Bindable
    public int getEpochDate() {
        return mEpochDate;
    }

    @Bindable
    public String getDate() {
        return mDate;
    }

    @Bindable
    public int getTemperatureMax() {
        return mTemperatureMax;
    }

    @Bindable
    public int getTemperatureMin() {
        return mTemperatureMin;
    }

    @Bindable
    public String getUnit() {
        return mUnit;
    }

    @Bindable
    public String getDayPhrase() {
        return mDayPhrase;
    }

    @Bindable
    public String getNightPhrase() {
        return mNightPhrase;
    }

    @Bindable
    public String getCity() {
        return mCity;
    }

    @Bindable
    public PropertyChangeRegistry getRegistry() {
        return mRegistry;
    }

    public void setEpochDate(final int epochDate) {
        this.mEpochDate = epochDate;
        mRegistry.notifyChange(this, BR.epochDate);
    }

    public void setDate(final String date) {
        this.mDate = date;
        mRegistry.notifyChange(this, BR.date);
    }

    public void setTemperatureMax(final int temperatureMax) {
        this.mTemperatureMax = temperatureMax;
        mRegistry.notifyChange(this, BR.temperatureMax);
    }

    public void setTemperatureMin(final int temperatureMin) {
        this.mTemperatureMin = temperatureMin;
        mRegistry.notifyChange(this, BR.temperatureMin);
    }

    public void setUnit(final String unit) {
        this.mUnit = unit;
        mRegistry.notifyChange(this, BR.unit);
    }

    public void setDayPhrase(final String dayPhrase) {
        this.mDayPhrase = dayPhrase;
        mRegistry.notifyChange(this, BR.dayPhrase);
    }

    public void setNightPhrase(final String nightPhrase) {
        this.mNightPhrase = nightPhrase;
        mRegistry.notifyChange(this, BR.nightPhrase);
    }

    public void setCity(final String city) {
        this.mCity = city;
        mRegistry.notifyChange(this, BR.city);
    }

    @Override
    public void addOnPropertyChangedCallback(@NonNull OnPropertyChangedCallback callback) {
        mRegistry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(@NonNull OnPropertyChangedCallback callback) {
        mRegistry.remove(callback);
    }

    @Override
    public String toString() {
        return "ObservableDailyForecast{" +
                "EpochDate=" + mEpochDate +
                ", Date='" + mDate + '\'' +
                ", TemperatureMax=" + mTemperatureMax +
                ", TemperatureMin=" + mTemperatureMin +
                ", TempUnit=" + mUnit + '\'' +
                ", DayPhrase='" + mDayPhrase + '\'' +
                ", NightPhrase='" + mNightPhrase + '\'' +
                ", City='" + mCity + '\'' +
                '}';
    }
}

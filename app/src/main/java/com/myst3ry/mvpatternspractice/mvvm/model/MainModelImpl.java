package com.myst3ry.mvpatternspractice.mvvm.model;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.util.Log;

import com.myst3ry.mvpatternspractice.MVPracticeApp;
import com.myst3ry.mvpatternspractice.data.local.DBManager;
import com.myst3ry.mvpatternspractice.data.local.model.ForecastModel;
import com.myst3ry.mvpatternspractice.data.local.model.ModelMapper;
import com.myst3ry.mvpatternspractice.data.remote.RetrofitHelper;
import com.myst3ry.mvpatternspractice.data.remote.pojo.City;
import com.myst3ry.mvpatternspractice.data.remote.pojo.DailyForecast;
import com.myst3ry.mvpatternspractice.data.remote.pojo.ForecastResponse;
import com.myst3ry.mvpatternspractice.utils.ResourcesManager;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class MainModelImpl implements MainModel {

    private static final String TAG = "MainModelImpl";
    private static final int CITY_FIRST_MATCH = 0;

    private OnForecastsReceivedListener mListener;
    private ResourcesManager mResourcesManager;
    private ExecutorService mExecutor;
    private RetrofitHelper mHelper;

    private static volatile MainModelImpl INSTANCE;

    public static MainModelImpl getInstance() {
        MainModelImpl instance = INSTANCE;
        if (instance == null) {
            synchronized (MainModelImpl.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new MainModelImpl();
                }
            }
        }
        return instance;
    }

    private MainModelImpl() {
        mResourcesManager = MVPracticeApp.getResourcesManager();
        mExecutor = Executors.newSingleThreadExecutor();
        mHelper = RetrofitHelper.getInstance();
    }

    @Override
    public void getForecasts(final OnForecastsReceivedListener listener) {
        this.mListener = listener;
        mExecutor.submit(this::getForecastsFromDb);
    }

    @Override
    public void getForecastsUpdates(final OnForecastsReceivedListener listener) {
        this.mListener = listener;
        getForecastsFromApi();
    }

    @WorkerThread
    private void getForecastsFromDb() {
        final List<ForecastModel> forecasts = DBManager.getInstance(null).getWeatherForecasts();
        if (forecasts != null && !forecasts.isEmpty()) {
            new Handler(Looper.getMainLooper()).post(() -> mListener.onForecastsReceived(forecasts));
        } else {
            getForecastsFromApi();
        }
    }

    @WorkerThread
    private void saveForecastsIntoDb(final List<ForecastModel> forecasts) {
        final DBManager manager = DBManager.getInstance(null);
        manager.removeWeatherForecasts();
        manager.saveWeatherForecasts(forecasts);
    }

    private void getForecastsFromApi() {
        mHelper.getService().getCitiesByQuery(RetrofitHelper.LOCATIONS_API_VERSION, RetrofitHelper.API_KEY,
                mResourcesManager.getCity(), mResourcesManager.getLanguage()).enqueue(new Callback<List<City>>() {
            @Override
            public void onResponse(@NonNull Call<List<City>> call, @NonNull Response<List<City>> response) {
                if (response.isSuccessful()) {
                    final List<City> cities = Objects.requireNonNull(response.body());
                    final City matchCity = cities.get(CITY_FIRST_MATCH);
                    getForecastsByLocation(matchCity.getKey());
                } else {
                    Log.e(TAG, response.code() + " " + response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<City>> call, @NonNull Throwable t) {
                t.printStackTrace();
                Log.e(TAG, t.getLocalizedMessage());
            }
        });
    }

    private void getForecastsByLocation(final String locationKey) {
        mHelper.getService().getWeatherForecast(RetrofitHelper.FORECASTS_API_VERSION, locationKey,
                RetrofitHelper.API_KEY, mResourcesManager.getLanguage(), mResourcesManager.isMetricEnabled())
                .enqueue(new Callback<ForecastResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<ForecastResponse> call, @NonNull Response<ForecastResponse> response) {
                        if (response.isSuccessful()) {
                            final List<DailyForecast> updates = Objects.requireNonNull(response.body()).getDailyForecasts();
                            final List<ForecastModel> forecasts = ModelMapper.transform(updates, mResourcesManager.getCity());
                            mExecutor.submit(() -> saveForecastsIntoDb(forecasts));
                            mListener.onForecastsReceived(forecasts);
                        } else {
                            Log.e(TAG, response.code() + " " + response.message());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ForecastResponse> call, @NonNull Throwable t) {
                        t.printStackTrace();
                        Log.e(TAG, t.getLocalizedMessage());
                    }
                });
    }
}

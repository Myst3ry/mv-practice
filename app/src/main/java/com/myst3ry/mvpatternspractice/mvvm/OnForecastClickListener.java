package com.myst3ry.mvpatternspractice.mvvm;

public interface OnForecastClickListener {

    void onForecastClick(final int date);
}

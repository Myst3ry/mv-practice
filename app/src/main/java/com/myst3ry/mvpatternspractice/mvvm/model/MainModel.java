package com.myst3ry.mvpatternspractice.mvvm.model;

import com.myst3ry.mvpatternspractice.data.local.model.ForecastModel;

import java.util.List;

public interface MainModel {

    void getForecasts(final OnForecastsReceivedListener listener);

    void getForecastsUpdates(final OnForecastsReceivedListener listener);

    interface OnForecastsReceivedListener {
        void onForecastsReceived(final List<ForecastModel> forecasts);
    }
}

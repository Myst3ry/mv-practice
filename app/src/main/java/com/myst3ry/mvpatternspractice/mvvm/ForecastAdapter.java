package com.myst3ry.mvpatternspractice.mvvm;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.myst3ry.mvpatternspractice.R;
import com.myst3ry.mvpatternspractice.databinding.ItemRecForecastBinding;
import com.myst3ry.mvpatternspractice.mvvm.databinding.ObservableDailyForecast;

import java.util.ArrayList;
import java.util.List;

public final class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastItemHolder> {

    private final OnForecastClickListener mListener;
    private List<ObservableDailyForecast> mForecastList;

    public ForecastAdapter(final OnForecastClickListener listener) {
        this.mForecastList = new ArrayList<>();
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ForecastItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final ItemRecForecastBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_rec_forecast, parent, false);
        return new ForecastItemHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastItemHolder holder, int position) {
        final ObservableDailyForecast forecast = getDailyForecast(position);
        holder.mBinding.setForecast(forecast);
        holder.mBinding.executePendingBindings();
        holder.mBinding.getRoot().setOnClickListener(v -> mListener.onForecastClick(forecast.getEpochDate()));
    }

    @Override
    public int getItemCount() {
        return mForecastList.size();
    }

    public void setForecastList(final List<ObservableDailyForecast> forecasts) {
        this.mForecastList = forecasts;
        notifyDataSetChanged();
    }

    private ObservableDailyForecast getDailyForecast(final int position) {
        return mForecastList.get(position);
    }

    final class ForecastItemHolder extends RecyclerView.ViewHolder {

        private ItemRecForecastBinding mBinding;

        ForecastItemHolder(ItemRecForecastBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }
    }
}

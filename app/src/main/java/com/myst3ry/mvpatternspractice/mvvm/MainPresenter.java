package com.myst3ry.mvpatternspractice.mvvm;

import android.util.Log;

import com.myst3ry.mvpatternspractice.data.local.model.ForecastModel;
import com.myst3ry.mvpatternspractice.mvvm.databinding.ObservableMapper;
import com.myst3ry.mvpatternspractice.mvvm.model.MainModel;
import com.myst3ry.mvpatternspractice.mvvm.model.MainModelImpl;
import com.myst3ry.mvpatternspractice.mvvm.view.MainView;

import java.util.List;

public final class MainPresenter<T extends MainView> {

    private static final String TAG = "MainPresenter";

    private T mView;
    private MainModel mModel;

    public MainPresenter() {
        mModel = MainModelImpl.getInstance();
    }

    public void attachView(T view) {
        this.mView = view;
    }

    public void getForecasts() {
        mView.showProgressBar();
        mModel.getForecasts(this::onForecastsReceived);
    }

    public void getForecastsUpdates() {
        mModel.getForecastsUpdates(this::onForecastsReceived);
    }

    private void onForecastsReceived(final List<ForecastModel> forecasts) {
        Log.w(TAG, "onForecastReceived: " + forecasts.size());

        if (!forecasts.isEmpty()) {
            mView.setForecasts(ObservableMapper.transform(forecasts));
        }
        mView.hideProgressBar();
        mView.hideRefresher();
    }

    public void detachView() {
        mView = null;
    }
}

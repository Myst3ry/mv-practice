package com.myst3ry.mvpatternspractice;

import android.app.Application;

import com.myst3ry.mvpatternspractice.data.local.DBManager;
import com.myst3ry.mvpatternspractice.utils.ResourcesManager;

public final class MVPracticeApp extends Application {

    private static ResourcesManager mResourcesManager;

    @Override
    public void onCreate() {
        super.onCreate();
        initResourcesManager();
        initDB();
    }

    private void initResourcesManager() {
        mResourcesManager = new ResourcesManager(this.getResources());
    }

    private void initDB() {
        DBManager.getInstance(this);
    }

    public static ResourcesManager getResourcesManager() {
        return mResourcesManager;
    }
}

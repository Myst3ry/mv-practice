package com.myst3ry.mvpatternspractice.data.remote.pojo;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public final class ForecastResponse{

	@SerializedName("DailyForecasts")
	private List<DailyForecast> dailyForecasts;

    public List<DailyForecast> getDailyForecasts(){
		return dailyForecasts;
	}
}
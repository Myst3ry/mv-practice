package com.myst3ry.mvpatternspractice.data.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class RetrofitHelper {

    public static final int FORECASTS_API_VERSION = 1;
    public static final int LOCATIONS_API_VERSION = 1;

    public static final String API_KEY = "rOPOdfxCsEiAgcoMEyThoGit9B9gToWT";
    private static final String BASE_URL = "http://dataservice.accuweather.com/";

    private static volatile RetrofitHelper INSTANCE;

    public static RetrofitHelper getInstance() {
        RetrofitHelper instance = INSTANCE;
        if (instance == null) {
            synchronized (RetrofitHelper.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new RetrofitHelper();
                }
            }
        }
        return instance;
    }

    private RetrofitHelper() { }

    public WeatherApi getService() {
        final Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(WeatherApi.class);
    }
}

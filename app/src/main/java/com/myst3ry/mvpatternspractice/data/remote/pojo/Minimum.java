package com.myst3ry.mvpatternspractice.data.remote.pojo;

import com.google.gson.annotations.SerializedName;

public final class Minimum{

	@SerializedName("Value")
	private float value;

	@SerializedName("Unit")
	private String unit;

	public float getValue(){
		return value;
	}

	public String getUnit(){
		return unit;
	}
}
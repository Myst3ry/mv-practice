package com.myst3ry.mvpatternspractice.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class DBHelper extends SQLiteOpenHelper {

    private final static int DB_VERSION = 1;
    private final static String DB_NAME = "weather_forecast_db";

    public DBHelper(final Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        createTables(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    private void createTables(final SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBContract.FORECASTS_TABLE + " (" + DBContract.COLUMN_EPOCH_DATE + " INTEGER PRIMARY KEY, "
                + DBContract.COLUMN_DATE + " TEXT, " + DBContract.COLUMN_TEMP_MIN + " INTEGER, " + DBContract.COLUMN_TEMP_MAX
                + " INTEGER, " + DBContract.COLUMN_TEMP_UNIT + " TEXT, " + DBContract.COLUMN_DAY_PHRASE + " TEXT, "
                + DBContract.COLUMN_NIGHT_PHRASE + " TEXT, " + DBContract.COLUMN_CITY + " TEXT)");
    }

    private void dropTables(final SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + DBContract.FORECASTS_TABLE);
    }

}

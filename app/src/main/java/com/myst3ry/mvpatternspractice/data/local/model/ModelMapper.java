package com.myst3ry.mvpatternspractice.data.local.model;

import com.myst3ry.mvpatternspractice.data.remote.pojo.DailyForecast;

import java.util.ArrayList;
import java.util.List;

public final class ModelMapper {

    public static ForecastModel transform(final DailyForecast forecast, final String city) {
        return new ForecastModel(
                forecast.getEpochDate(),
                forecast.getDate(),
                Math.round(forecast.getTemperature().getMaximum().getValue()),
                Math.round(forecast.getTemperature().getMinimum().getValue()),
                forecast.getTemperature().getMinimum().getUnit(),
                forecast.getDay().getIconPhrase(),
                forecast.getNight().getIconPhrase(),
                city);
    }

    public static List<ForecastModel> transform(final List<DailyForecast> forecasts, final String city) {
        final List<ForecastModel> models = new ArrayList<>();
            for (final DailyForecast forecast : forecasts) {
                models.add(transform(forecast, city));
            }

        return models;
    }

    private ModelMapper() { }
}

package com.myst3ry.mvpatternspractice.utils;

import android.content.res.Resources;

import com.myst3ry.mvpatternspractice.R;

public final class ResourcesManager {

    private Resources mResources;

    public ResourcesManager(final Resources resources) {
        this.mResources = resources;
    }

    public String getCountry() {
        return mResources.getString(R.string.country);
    }

    public String getCity() {
        return mResources.getString(R.string.city);
    }

    public String getLanguage() {
        return mResources.getString(R.string.language);
    }

    public boolean isMetricEnabled() {
        return mResources.getBoolean(R.bool.metrics);
    }
}

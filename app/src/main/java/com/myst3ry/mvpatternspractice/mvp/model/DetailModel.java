package com.myst3ry.mvpatternspractice.mvp.model;

import com.myst3ry.mvpatternspractice.data.local.model.ForecastModel;

public interface DetailModel {

    void getWeatherForecastFromDb(final int date, final OnForecastReceivedListener listener);

    interface OnForecastReceivedListener {
        void onForecastReceived(final ForecastModel forecast);
    }
}

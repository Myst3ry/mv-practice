package com.myst3ry.mvpatternspractice.mvp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.myst3ry.mvpatternspractice.BuildConfig;
import com.myst3ry.mvpatternspractice.R;
import com.myst3ry.mvpatternspractice.mvp.presenter.DetailPresenter;
import com.myst3ry.mvpatternspractice.mvvm.databinding.ObservableDailyForecast;
import com.myst3ry.mvpatternspractice.utils.DateUtils;
import com.myst3ry.mvpatternspractice.utils.StringUtils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class DetailActivity extends AppCompatActivity implements DetailView {

    private static final String EXTRA_EPOCH_DATE = BuildConfig.APPLICATION_ID + "extra.EPOCH_DATE";

    @BindView(R.id.forecast_city)
    TextView mCityText;
    @BindView(R.id.forecast_day_of_week)
    TextView mWeekdayText;
    @BindView(R.id.forecast_date)
    TextView mDateText;
    @BindView(R.id.forecast_max_temp)
    TextView mMaxTempText;
    @BindView(R.id.forecast_day_phrase)
    TextView mDayPhraseText;
    @BindView(R.id.forecast_min_temp)
    TextView mMinTempText;
    @BindView(R.id.forecast_night_phrase)
    TextView mNightPhraseText;

    private DetailPresenter<DetailActivity> mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast_detail);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initPresenter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        final int date = getIntent().getIntExtra(EXTRA_EPOCH_DATE, 0);
        setTitle(DateUtils.getDayOfWeek(date));
        mPresenter.getWeatherForecast(date);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setWeatherForecastInfo(final ObservableDailyForecast forecast) {
        mCityText.setText(forecast.getCity());
        mWeekdayText.setText(DateUtils.getDayOfWeek(forecast.getEpochDate()));
        mDateText.setText(DateUtils.parseDate(forecast.getDate()));
        mMaxTempText.setText(StringUtils.getTempUnitString(forecast.getTemperatureMax(), forecast.getUnit()));
        mMinTempText.setText(StringUtils.getTempUnitString(forecast.getTemperatureMin(), forecast.getUnit()));
        mDayPhraseText.setText(forecast.getDayPhrase());
        mNightPhraseText.setText(forecast.getNightPhrase());
    }

    private void initPresenter() {
        mPresenter = new DetailPresenter<>();
        mPresenter.attachView(this);
    }

    public static Intent newExtraIntent(final Context context, final int date) {
        final Intent intent = newIntent(context);
        intent.putExtra(EXTRA_EPOCH_DATE, date);
        return intent;
    }

    public static Intent newIntent(final Context context) {
        return new Intent(context, DetailActivity.class);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isDestroyed()) {
            mPresenter.detachView();
        }
    }
}

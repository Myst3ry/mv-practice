package com.myst3ry.mvpatternspractice.mvp.presenter;

import com.myst3ry.mvpatternspractice.data.local.model.ForecastModel;
import com.myst3ry.mvpatternspractice.mvp.view.DetailView;
import com.myst3ry.mvpatternspractice.mvp.model.DetailModel;
import com.myst3ry.mvpatternspractice.mvp.model.DetailModelImpl;
import com.myst3ry.mvpatternspractice.mvvm.databinding.ObservableMapper;

public final class DetailPresenter<T extends DetailView> {

    private T mView;
    private DetailModel mModel;

    public DetailPresenter() {
        mModel = DetailModelImpl.getInstance();
    }

    public void attachView(T view) {
        this.mView = view;
    }

    public void getWeatherForecast(final int date) {
        mModel.getWeatherForecastFromDb(date, this::onForecastReceived);
    }

    private void onForecastReceived(final ForecastModel forecast) {
        mView.setWeatherForecastInfo(ObservableMapper.transform(forecast));
    }

    public void detachView() {
        this.mView = null;
    }

}

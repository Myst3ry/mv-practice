package com.myst3ry.mvpatternspractice.mvp.model;

import android.os.Handler;
import android.os.Looper;

import com.myst3ry.mvpatternspractice.data.local.DBManager;
import com.myst3ry.mvpatternspractice.data.local.model.ForecastModel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class DetailModelImpl implements DetailModel {

    private static volatile DetailModelImpl INSTANCE;

    public static DetailModelImpl getInstance() {
        DetailModelImpl instance = INSTANCE;
        if (instance == null) {
            synchronized (DetailModelImpl.class) {
                instance = INSTANCE;
                if (instance == null) {
                    instance = INSTANCE = new DetailModelImpl();
                }
            }
        }
        return instance;
    }

    private DetailModelImpl() { }

    @Override
    public void getWeatherForecastFromDb(int date, final OnForecastReceivedListener listener) {
        final ExecutorService executor = Executors.newSingleThreadExecutor();
        final Handler handler = new Handler(Looper.getMainLooper());
        executor.submit(() -> {
            final ForecastModel forecast = DBManager.getInstance(null).getDailyForecast(date);
            handler.post(() -> listener.onForecastReceived(forecast));
        });
    }
}

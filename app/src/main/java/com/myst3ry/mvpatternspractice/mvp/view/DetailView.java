package com.myst3ry.mvpatternspractice.mvp.view;

import com.myst3ry.mvpatternspractice.mvvm.databinding.ObservableDailyForecast;

public interface DetailView {

    void setWeatherForecastInfo(final ObservableDailyForecast forecast);
}
